<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DemoController extends Controller
{
    public function login(Request $request)
    {
    	return view('login');
    }
    public function register(Request $request)
    {
    	return view('register');
    }
    public function getData(Request $request)
    {
        $usernm = $request->first;
        $pass = $request->second;
        $data = $request->all();
        View::share('MyData',$data);
        return view($this->viewPath.'formdata',compact('data'));
    }
}
